#!/bin/bash
set -e

HE_DIR=$1
HOST_NAME=$2

PROPERTY_FILE=./bcbsne_env.properties

function getProperty {
   PROP_KEY=$1
   PROP_VALUE=`
   cat $PROPERTY_FILE | grep "$PROP_KEY" | cut -d'=' -f2`
   echo $PROP_VALUE
}

echo ""
echo "********** BCBSNE SH : bcbsne_positive_pay_configure.sh start on ${HOST_NAME} **********"


    echo "Start configuring positive pay job"

        positivepayJsonConfigFile=$HE_DIR/bcbsne-positivepay/resources/config/positivepay-job-config.json
        positivepayJsonRouteFile=$HE_DIR/bcbsne-positivepay/resources/config/positivepay-route-config.json

        chmod -R 755 $positivepayJsonConfigFile
        chmod -R 755 $positivepayJsonRouteFile

        sed -i -e "s|#HE_DIR#|${HE_DIR}|g" $positivepayJsonConfigFile

        extractStartEndTime=$(date +"%Y-%m-%dT%T.%3NEDT")
        echo "extractStartEndTime: ${extractStartEndTime}"

        sed -i -e "s|SSSS-MM-DDTHH:MM:SS.SSSZEDT|${extractStartEndTime}|g" $positivepayJsonConfigFile
        sed -i -e "s|EEEE-MM-DDTHH:MM:SS.SSSZEDT|${extractStartEndTime}|g" $positivepayJsonConfigFile

        extractOutputPath=$(getProperty positive_pay_extract_output_path)
        echo "extractOutputPath: ${extractOutputPath}"

        sed -i -e "s|#EXTRACT_OUTPUT_PATH#|${extractOutputPath}|g" $positivepayJsonConfigFile

        extractConfigRequestDir=$(grep "extractConfigRequestDir" $HE_DIR/etc/com.healthedge.customer.generic.configuration.cfg | sed -e "s/extractConfigRequestDir=//g")
        echo "extractConfigRequestDir: ${extractConfigRequestDir}"

        echo "Dropping positive pay job configuration json ${positivepayJsonConfigFile} to ${extractConfigRequestDir}"
        cp $positivepayJsonConfigFile $extractConfigRequestDir

    echo "End configuring positive pay job"


    echo "Start configuring positive pay job trigger route"

        extractCron=$(getProperty positive_pay_extract_job_trigger_cron)
        echo "extractCron: ${extractCron}"

        sed -i -e "s|#EXTRACT_TIME#|${extractCron}|g" $positivepayJsonRouteFile

        fileRequestBaseDir=$(grep "fileRequestBaseDir" $HE_DIR/etc/com.healthedge.customer.generic.scheduler.cfg | sed -e "s/fileRequestBaseDir=//g")
        fileRequestBaseDir=${fileRequestBaseDir}request/cron

        echo "Dropping positive pay job trigger creation json ${positivepayJsonRouteFile} to ${fileRequestBaseDir}"
        cp $positivepayJsonRouteFile $fileRequestBaseDir

    echo "End configuring positive pay job trigger route"


echo "********** BCBSNE SH : bcbsne_positive_pay_configure.sh end on ${HOST_NAME} **********"
echo ""